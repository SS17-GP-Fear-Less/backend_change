#!/usr/bin/python
import requests as r
import os
import json
import sys

# res = r.get('http://0.0.0.0:8081/pattern', timeout=2);
# text = res.text
# print (text)

url = 'http://0.0.0.0:8081/'
data = json.dumps({})
output = r.post(url, data = data)


## CREATE USER
url = 'http://0.0.0.0:8081/register'
data = json.dumps({"username": "test", "mail": "mail@mail.com", "password": "test0"})
output = r.post(url, data = data)

## CHANGE PASS
url = 'http://0.0.0.0:8081/users/pass'
data = json.dumps({"username": "test", "password": "test0", "new_password": "test"})
output = r.post(url, data = data)

## MAKE USER ADMIN
url = 'http://0.0.0.0:8081/users/admin'
data = json.dumps({"username": "test", "password": "test", "master_password": "admin!"})
output = r.post(url, data = data)

## GENERATE TOKEN
url = 'http://0.0.0.0:8081/login'
data = json.dumps({"username": "test", "password": "test"})
output = r.post(url, data = data)

js = str(output.text).replace("b'", "").replace("'", "")
j = json.loads(js)

# for key, value in j.items():
#         token = value
token = j["token"]
print (token)

## ADD PATTERN1
url = 'http://0.0.0.0:8081/patterns'
data = json.dumps({"token": ""+token+"", "Title" : "Help the Positive","Description" : "Long Description Text","Problem" : "motivate them with a sense of hope rather than fear.", "Solution" : "Your attempts to scare others are not working."})
output = r.post(url, data = data)


## ADD PATTERN2
url = 'http://0.0.0.0:8081/patterns'
data = json.dumps({"token": ""+token+"", "Title" : "Help the Positive2","Description" : "Long Description Text","Problem" : "motivate them with a sense of hope rather than fear.", "Solution" : "Your attempts to scare others are not working."})
output = r.post(url, data = data)

## PRINT PATTERN
url = 'http://0.0.0.0:8081/patterns'
payload = {'token': token}
output = r.get(url, params=payload)

# output = r.get('curl  http://0.0.0.0:8081/pattern')
js = str(output.text).replace("b'", "").replace("'", "")
# print (js)
j = json.loads(js)
ids = []

print ("========================")
for jj in j:
    ids.append(jj['_id'])
    print (jj['_id'])
    # for key, value in jj.items():
    #     pass
        # print (token, value)

print ("========================")
## ADD ALL PATTERN FAV
for idd in ids:
        url = 'http://0.0.0.0:8081/users/favorites/patterns'
        data = json.dumps({"token": ""+token+"", "id": ""+idd+""})
        output = r.post(url, data = data)

## PRINT FAV
url = 'http://0.0.0.0:8081/users/favorites/patterns'
data = json.dumps({"token": ""+token+""})
output = r.get(url, data = data)

js = str(output.text).replace("b'", "").replace("'", "")
print (js)

# ADD SCENARIOS
print ("ADD SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios'
data = json.dumps({"token": ""+token+"", "title": "titledesc1", "description": "scen1"})
print (data)
output = r.post(url, data = data)
print (output)

## PRINT SCENARIOS
print ("PRINT SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios'
payload = {'token': token}
output = r.get(url, params=payload)
js = str(output.text).replace("b'", "").replace("'", "")
s = json.loads(js)
print (s[0]["_id"])
idScen = s[0]["_id"]

# ADD STRATEGIE TO SCENARIOS
print ("ADD STRATEGIE TO SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios/strategies'
data = json.dumps({"token": ""+token+"", "idScenario": ""+idScen+"", "strategies": "123,321"})
output = r.post(url, data = data)
print (output)

## PRINT SCENARIOS
print ("PRINT SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios'
payload = {'token': token}
output = r.get(url, params=payload)
print(output.text)

# DEL STRATEGIE FROM SCENARIOS
print ("DEL STRATEGIE FROM SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios/strategies'
data = json.dumps({"token": ""+token+"", "idScenario": ""+idScen+"", "strategies": "123,321"})
output = r.delete(url, data = data)
print (output)

## PRINT SCENARIOS
print ("PRINT SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios'
payload = {'token': token}
output = r.get(url, params=payload)
print(output.text)
# sys.exit(0)

# DELETE SCENARIOS
print ("DELETE SCENARIOS")
url = 'http://0.0.0.0:8081/users/scenarios'
data = json.dumps({"token": ""+token+"", "id": ""+idScen+""})
output = r.delete(url, data = data)
print (output)

# DELETE 1 FAV
for idd in ids:
        url = 'http://0.0.0.0:8081/users/favorites/patterns'
        data = json.dumps({"username": "test", "token": ""+token+"", "id": ""+idd+""})
        output = r.delete(url, data = data)


## PRINT FAV AGAIN
print ("FAV EMPTY")
url = 'http://0.0.0.0:8081/users/favorites/patterns'
data = json.dumps({"token": ""+token+""})
output = r.get(url, data = data)

js = str(output.text).replace("b'", "").replace("'", "")
print (js)

## DELETE ALL PATTERN

for idd in ids:
    print (idd)
    url = 'http://0.0.0.0:8081/patterns'
    data = json.dumps({"token": ""+token+"", "id": ""+idd+""})
    output = r.delete(url, data = data)

    js = str(output.text).replace("b'", "").replace("'", "")
    print (js)



## DELETE USER
url = 'http://0.0.0.0:8081/users'
data = json.dumps({"username": "test", "password": "test"})
output = r.delete(url, data = data)




# PATTERN EXAMPLE
# "username": "test",
# "password": "test",
# "Title" : "Help the Positive",
# "Description" : "Long Description Text",
# "Problem" : "motivate them with a sense of hope rather than fear.",
# "Solution" : "Your attempts to scare others are not working."
