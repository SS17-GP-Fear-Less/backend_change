package util;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

public class MongoDbObjectIdToId {

    public static JsonArray ListJsonObjToJsonArray(List<JsonObject> result){
       JsonArray jsonArray = new JsonArray();
        for (JsonObject obj :
                result) {
            jsonArray.add(ReplaceOidWithId(obj));
        }
        return jsonArray;

    }

    public static JsonObject ReplaceOidWithId(JsonObject obj) {
        JsonObject result = obj.copy();
        String id = obj.getJsonObject("_id").getString("$oid");
        result.remove("_id");
        result.put("id",id);
        return result;
    }
}
