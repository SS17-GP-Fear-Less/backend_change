package util;

import com.mongodb.MongoException;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;
import mongo.Client;

public class AuthenticationHelper {

    Handler<RoutingContext> handler;

    public AuthenticationHelper(Handler<RoutingContext> handler) {
        this.handler = handler;
    }

    public void isAdminBeforeHanlde(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        String token = requestAsJson.getString("token");

        JsonObject query = new JsonObject().put("token", token).put("admin", 1);

        mongoClient.findOne("users", query, null, res -> {
            System.out.println("res=" + res);
            System.out.println("con=" + context);
            this.CallHandlerOnSuccessOrError(res, context);
        });
    }

    public void isUserBeforeHanlde(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        String token = requestAsJson.getString("token");

        JsonObject query = new JsonObject().put("token", token);
        System.out.println(query.encodePrettily());
        try {
            mongoClient.findOne("users", query, null, res -> {
                this.CallHandlerOnSuccessOrError(res, context);
            });
        } catch (MongoException e) {
            JsonObject jsonResposne = new JsonObject().put("error", "Bad login");
            response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code());
            response.end(jsonResposne.encode());
        }

    }

    private void CallHandlerOnSuccessOrError(AsyncResult<JsonObject> res, RoutingContext context) {
        if (res.result() != null && res.succeeded() && !res.result().isEmpty()) {
            handler.handle(context);
        } else {
            HttpServerResponse response = context.response();
            JsonObject jsonRespone = new JsonObject().put("error", "Not Admin or failed to connect");
            response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code());
            response.end(jsonRespone.encode());
        }
    }
}
