package mongo;


import io.vertx.core.json.JsonObject;

import java.util.ArrayList;

/**
 * This class is our MongoDB configuration. Must be created with a Builder.
 */
public class MongoDBConfiguration {

    private final JsonObject config;

    // Single Cluster Settings
    private final String host;                            // default value: 127.0.0.1
    private final Integer port;                           // default value: 27017

    // Multiple Cluster Settings
    private final ArrayList<Host> hosts;
    private final String replicaSet;
    private final Long serverSelectionTimeoutMS;

    static class Host {
        final String host;
        final int port;

        Host(String host, int port) {
            this.host = host;
            this.port = port;
        }
    }

    // Connection Pool Settings
    private final Integer maxPoolSize;                    // default value: 100
    private final Integer minPoolSize;                    // default value: 0
    private final Long maxIdleTimeMS;                     // default value: 0
    private final Long maxLifeTimeMS;                     // default value: 0
    private final Integer waitQueueMultiple;              // default value: 500
    private final Long waitQueueTimeoutMS;                // default value: 120000
    private final Long maintenanceFrequencyMS;            // default value: 0
    private final Long maintenanceInitialDelayMS;         // default value: 0

    // Credentials / Auth
    private final String username;                        // default value: null (meaning no authentication required)
    private final String password;
    private final String authSource;                      // default value: db_name
    private final String authMechanism;
    private final String gssapiServiceName;

    // Socket Settings
    private final Integer connectTimeoutMS;               // default value: 10000
    private final Integer socketTimeoutMS;                // default value: 0
    private final Integer sendBufferSize;                 // default value: 0
    private final Integer receiveBufferSize;              // default value: 0
    private final Boolean keepAlive;                      // default value: false

    private final HeartbeatSocket heartbeatSocket;

    // Heartbeat socket settings
    static class HeartbeatSocket {
        private Integer connectTimeoutMS;
        private Integer socketTimeoutMS;
        private Integer sendBufferSize;
        private Integer receiveBufferSize;
        private Boolean keepAlive;

        HeartbeatSocket(HeartbeatSocketBuilder builder) {
            this.connectTimeoutMS = builder.connectTimeoutMS;
            this.socketTimeoutMS = builder.socketTimeoutMS;
            this.sendBufferSize = builder.sendBufferSize;
            this.receiveBufferSize = builder.receiveBufferSize;
            this.keepAlive = builder.keepAlive;
        }
        boolean isEmpty() {
            return connectTimeoutMS == null && socketTimeoutMS != null
                    && sendBufferSize == null && receiveBufferSize == null
                    && keepAlive == null;
        }
    }

    // Server Settings
    private Long heartbeatFrequencyMS;              // default value: 1000
    private Long minHeartbeatFrequencyMS;           // default value: 500


    MongoDBConfiguration(MongoDBConfigurationBuilder builder) {
        config = new JsonObject();

        this.host = builder.host;
        this.port = builder.port;
        this.hosts = builder.hosts;
        this.replicaSet = builder.replicaSet;
        this.serverSelectionTimeoutMS = builder.serverSelectionTimeoutMS;
        this.maxPoolSize = builder.maxPoolSize;
        this.minPoolSize = builder.minPoolSize;
        this.maxIdleTimeMS = builder.maxIdleTimeMS;
        this.maxLifeTimeMS = builder.maxLifeTimeMS;
        this.waitQueueMultiple = builder.waitQueueMultiple;
        this.waitQueueTimeoutMS = builder.waitQueueTimeoutMS;
        this.maintenanceFrequencyMS = builder.maintenanceFrequencyMS;
        this.maintenanceInitialDelayMS = builder.maintenanceInitialDelayMS;
        this.username = builder.username;
        this.password = builder.password;
        this.authSource = builder.authSource;
        this.authMechanism = builder.authMechanism;
        this.gssapiServiceName = builder.gssapiServiceName;
        this.connectTimeoutMS = builder.connectTimeoutMS;
        this.socketTimeoutMS = builder.socketTimeoutMS;
        this.sendBufferSize = builder.sendBufferSize;
        this.receiveBufferSize = builder.receiveBufferSize;
        this.keepAlive = builder.keepAlive;
        this.heartbeatSocket = builder.heartbeatSocket;
        this.heartbeatFrequencyMS = builder.heartbeatFrequencyMS;
        this.minHeartbeatFrequencyMS = builder.minHeartbeatFrequencyMS;
    }

    private <E> void putIfNotNull(String key, E element) {
        if (element != null) {
            config.put(key, element);
        }
    }

    /**
     * @return the jsonObject of the configuration.
     */
    public JsonObject toJsonObject() {

        // Used to create the jsonObject only the
        if (config.isEmpty()) {
            putIfNotNull("host", host);
            putIfNotNull("port", port);
            if (!hosts.isEmpty()) {
                JsonObject hostsJson = new JsonObject();
                for (Host hostObject : hosts) {
                    hostsJson.put("host", hostObject.host);
                    hostsJson.put("port", hostObject.port);
                }
                config.put("hosts", hostsJson);
            }

            if (!heartbeatSocket.isEmpty()) {
                JsonObject heartbeatSocketJson = new JsonObject();
                if (heartbeatSocket.connectTimeoutMS != null) heartbeatSocketJson.put("connectTimeoutMS", heartbeatSocket.connectTimeoutMS);
                if (heartbeatSocket.socketTimeoutMS != null) heartbeatSocketJson.put("socketTimeoutMS", heartbeatSocket.socketTimeoutMS);
                if (heartbeatSocket.sendBufferSize != null) heartbeatSocketJson.put("sendBufferSize", heartbeatSocket.sendBufferSize);
                if (heartbeatSocket.receiveBufferSize != null) heartbeatSocketJson.put("receiveBufferSice", heartbeatSocket.receiveBufferSize);
                if (heartbeatSocket.keepAlive != null) heartbeatSocketJson.put("keepAlive", heartbeatSocket.keepAlive);
                config.put("heartbeat.socket", heartbeatSocketJson);
            }

            putIfNotNull("replicaSet", replicaSet);
            putIfNotNull("serverSelectionTimeoutMS", serverSelectionTimeoutMS);
            putIfNotNull("maxPoolSize", maxPoolSize);
            putIfNotNull("minPoolSize", minPoolSize);
            putIfNotNull("maxIdleTimeMS", maxIdleTimeMS);
            putIfNotNull("maxLifeTimeMS", maxLifeTimeMS);
            putIfNotNull("waitQueueMultiple", waitQueueMultiple);
            putIfNotNull("waitQueueTimeout", waitQueueTimeoutMS);
            putIfNotNull("maintenanceFrequencyMS", maintenanceFrequencyMS);
            putIfNotNull("maintenanceInitialDelayMS", maintenanceInitialDelayMS);
            putIfNotNull("username", username);
            putIfNotNull("password", password);
            putIfNotNull("authSource", authSource);
            putIfNotNull("authMechanism", authMechanism);
            putIfNotNull("gssapiServiceName", gssapiServiceName);
            putIfNotNull("connectTimeoutMS", connectTimeoutMS);
            putIfNotNull("socketTimeoutMS", socketTimeoutMS);
            putIfNotNull("sendBufferSize", sendBufferSize);
            putIfNotNull("receiveBufferSize", receiveBufferSize);
            putIfNotNull("keepAlive", keepAlive);
            putIfNotNull("heartbeatFrequencyMS", heartbeatFrequencyMS);
            putIfNotNull("minHeartbeatFrequencyMS", heartbeatFrequencyMS);

            // This line is used to not rebuild the config, even is all element is not set.
            config.put("__metadata__", "build");
        }
        return config;
    }

}
