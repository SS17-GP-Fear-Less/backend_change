package mongo;

/**
 * A part of the MongoDBConfigurationBuilder.
 * Build the HeartBeatSocket configuration.
 */
class HeartbeatSocketBuilder {
    Integer connectTimeoutMS;
    Integer socketTimeoutMS;
    Integer sendBufferSize;
    Integer receiveBufferSize;
    Boolean keepAlive;

    HeartbeatSocketBuilder connectTimeoutMS(Integer connectTimeoutMS) {
        this.connectTimeoutMS = connectTimeoutMS;
        return this;
    }

    HeartbeatSocketBuilder socketTimeoutMS(Integer socketTimeoutMS) {
        this.socketTimeoutMS = socketTimeoutMS;
        return this;
    }

    HeartbeatSocketBuilder sendBufferSize(Integer sendBufferSize) {
        this.sendBufferSize = sendBufferSize;
        return this;
    }

    HeartbeatSocketBuilder receiveBufferSize(Integer receiveBufferSize) {
        this.receiveBufferSize = receiveBufferSize;
        return this;
    }

    HeartbeatSocketBuilder keepAlive(Boolean keepAlive) {
        this.keepAlive = keepAlive;
        return this;
    }

    MongoDBConfiguration.HeartbeatSocket build() {
        return new MongoDBConfiguration.HeartbeatSocket(this);
    }
}