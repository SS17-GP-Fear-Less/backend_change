package mongo;

import java.util.ArrayList;

/**
 * This class is a builder of MongoDBConfiguration.
 * We use a builder to simplify the interface of the config creation.
 */
public class MongoDBConfigurationBuilder {
    private final HeartbeatSocketBuilder builder;
    String host;
    Integer port;
    ArrayList<MongoDBConfiguration.Host> hosts = new ArrayList<>();
    String replicaSet;
    Long serverSelectionTimeoutMS;
    Integer maxPoolSize;
    Integer minPoolSize;
    Long maxIdleTimeMS;
    Long maxLifeTimeMS;
    Integer waitQueueMultiple;
    Long waitQueueTimeoutMS;
    Long maintenanceFrequencyMS;
    Long maintenanceInitialDelayMS;
    String username;
    String password;
    String authSource;
    String authMechanism;
    String gssapiServiceName;
    Integer connectTimeoutMS;
    Integer socketTimeoutMS;
    Integer sendBufferSize;
    Integer receiveBufferSize;
    Boolean keepAlive;
    MongoDBConfiguration.HeartbeatSocket heartbeatSocket;
    Long heartbeatFrequencyMS;
    Long minHeartbeatFrequencyMS;

    public MongoDBConfigurationBuilder(){
        builder = new HeartbeatSocketBuilder();
    }

    public MongoDBConfigurationBuilder heartbeatSocketConnectTimeoutMS(Integer connectTimeoutMS) {
        builder.connectTimeoutMS = connectTimeoutMS;
        return this;
    }

    public MongoDBConfigurationBuilder heartbeatSocketSocketTimeoutMS(Integer socketTimeoutMS) {
        builder.socketTimeoutMS = socketTimeoutMS;
        return this;
    }

    public MongoDBConfigurationBuilder heartbeatSocketSendBufferSize(Integer sendBufferSize) {
        builder.sendBufferSize = sendBufferSize;
        return this;
    }

    public MongoDBConfigurationBuilder heartbeatSocketReceiveBufferSize(Integer receiveBufferSize) {
        builder.receiveBufferSize = receiveBufferSize;
        return this;
    }

    public MongoDBConfigurationBuilder heartbeatSocketKeepAlive(Boolean keepAlive) {
        builder.keepAlive = keepAlive;
        return this;
    }


    public MongoDBConfigurationBuilder host(String host) {
        this.host = host;
        return this;
    }

    public MongoDBConfigurationBuilder port(Integer port) {
        this.port = port;
        return this;
    }

    public MongoDBConfigurationBuilder addHosts(String host, int port) {
        hosts.add(new MongoDBConfiguration.Host(host, port));
        return this;
    }

    public MongoDBConfigurationBuilder replicaSet(String replicaSet) {
        this.replicaSet = replicaSet;
        return this;
    }

    public MongoDBConfigurationBuilder serverSelectionTimeoutMS(Long serverSelectionTimeoutMS) {
        this.serverSelectionTimeoutMS = serverSelectionTimeoutMS;
        return this;
    }

    public MongoDBConfigurationBuilder maxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
        return this;
    }

    public MongoDBConfigurationBuilder minPoolSize(Integer minPoolSize) {
        this.minPoolSize = minPoolSize;
        return this;
    }

    public MongoDBConfigurationBuilder maxIdleTimeMS(Long maxIdleTimeMS) {
        this.maxIdleTimeMS = maxIdleTimeMS;
        return this;
    }

    public MongoDBConfigurationBuilder maxLifeTimeMS(Long maxLifeTimeMS) {
        this.maxLifeTimeMS = maxLifeTimeMS;
        return this;
    }

    public MongoDBConfigurationBuilder waitQueueMultiple(Integer waitQueueMultiple) {
        this.waitQueueMultiple = waitQueueMultiple;
        return this;
    }

    public MongoDBConfigurationBuilder waitQueueTimeoutMS(Long waitQueueTimeoutMS) {
        this.waitQueueTimeoutMS = waitQueueTimeoutMS;
        return this;
    }

    public MongoDBConfigurationBuilder maintenanceFrequencyMS(Long maintenanceFrequencyMS) {
        this.maintenanceFrequencyMS = maintenanceFrequencyMS;
        return this;
    }

    public MongoDBConfigurationBuilder maintenanceInitialDelayMS(Long maintenanceInitialDelayMS) {
        this.maintenanceInitialDelayMS = maintenanceInitialDelayMS;
        return this;
    }

    public MongoDBConfigurationBuilder username(String username) {
        this.username = username;
        return this;
    }

    public MongoDBConfigurationBuilder password(String password) {
        this.password = password;
        return this;
    }

    public MongoDBConfigurationBuilder authSource(String authSource) {
        this.authSource = authSource;
        return this;
    }

    public MongoDBConfigurationBuilder authMechanism(String authMechanism) {
        this.authMechanism = authMechanism;
        return this;
    }

    public MongoDBConfigurationBuilder gssapiServiceName(String gssapiServiceName) {
        this.gssapiServiceName = gssapiServiceName;
        return this;
    }

    public MongoDBConfigurationBuilder connectTimeoutMS(Integer connectTimeoutMS) {
        this.connectTimeoutMS = connectTimeoutMS;
        return this;
    }

    public MongoDBConfigurationBuilder setSocketTimeoutMS(Integer socketTimeoutMS) {
        this.socketTimeoutMS = socketTimeoutMS;
        return this;
    }

    public MongoDBConfigurationBuilder setSendBufferSize(Integer sendBufferSize) {
        this.sendBufferSize = sendBufferSize;
        return this;
    }

    public MongoDBConfigurationBuilder setReceiveBufferSize(Integer receiveBufferSize) {
        this.receiveBufferSize = receiveBufferSize;
        return this;
    }

    public MongoDBConfigurationBuilder setKeepAlive(Boolean keepAlive) {
        this.keepAlive = keepAlive;
        return this;
    }

    public MongoDBConfigurationBuilder setHeartbeatFrequencyMS(Long heartbeatFrequencyMS) {
        this.heartbeatFrequencyMS = heartbeatFrequencyMS;
        return this;
    }

    public MongoDBConfigurationBuilder setMinHeartbeatFrequencyMS(Long minHeartbeatFrequencyMS) {
        this.minHeartbeatFrequencyMS = minHeartbeatFrequencyMS;
        return this;
    }

    public MongoDBConfiguration build() {
        heartbeatSocket = builder.build();
        return new MongoDBConfiguration(this);
    }
}