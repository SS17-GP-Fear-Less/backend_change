package mongo;

import io.vertx.core.json.Json;
import mongo.MongoDBConfiguration;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import mongo.MongoDBConfiguration;

/**
 * Singleton instance used to keep our mongoDB client.
 */


public enum Client {

    INSTANCE;


    private JsonObject config = new JsonObject()
            .put("db_name", "ANDROID")
            .put("useObjectId", false)
            .put("host", "mongodb");


    private MongoClient client;

    public void init(Vertx vertx) {
        client = MongoClient.createShared(vertx, config);

    }



    public MongoClient getMongoClient() {

        return client;
    }

    public JsonObject getConfig() {
        return config;
    }
}