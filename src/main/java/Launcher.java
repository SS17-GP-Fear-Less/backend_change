import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * The entry point of the backend programme.
 */
public class Launcher {

    private static final Logger logger = LoggerFactory.getLogger(Launcher.class);


    // Convenience method so you can run it in your IDE
    public static void main(String[] args) {

        logger.debug("test");

        // Get number or processors of the machine
        int instances = Runtime.getRuntime().availableProcessors();

        // Set the number of instance the same as the number of processors
        DeploymentOptions opt = new DeploymentOptions().setInstances(instances);

        opt.setConfig(new JsonObject());
        opt.getConfig().put("db", "127.0.0.1");

        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle("HttpVerticle", opt);
    }


}
