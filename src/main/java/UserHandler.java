import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;
import jdk.nashorn.api.scripting.JSObject;
import mongo.Client;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

public class UserHandler {

     public static String generateString() {
         String uuid = UUID.randomUUID().toString();
         return uuid.replace("-", "");

     }

     public static String getUserbyKey(String token) {
         final AtomicReference<String> reference = new AtomicReference<String>();
         MongoClient mongoClient = Client.INSTANCE.getMongoClient();

         JsonObject query = new JsonObject().put("token", token);

         mongoClient.find("users", query, res -> {
             if (res.succeeded()) {
                 System.out.println("AAAAAAAAAAAAAAAAAA");

                 reference.set(res.result().get(0).getString("_id"));
                 System.out.println(reference.get());

             }
         });
         String userId = new String();
         System.out.println(reference.get());
         return userId;
     }
     /**
      ** curl -H "Content-Type: application/json" -X GET -d '{"token": "1234"}' http://0.0.0.0:8081/user
      **/
    public void getAllUser(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        String token = requestAsJson.getString("token");

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();
        JsonObject query = new JsonObject().put("token", token).put("admin", 1);

        mongoClient.find("users", query, resUser -> {
            try {
                if (resUser.succeeded() && !resUser.result().get(0).isEmpty()) {


                    mongoClient.find("users", new JsonObject(), res -> {
                        if (res.succeeded()) {
                            JsonArray users = new JsonArray();
                            for (JsonObject r : res.result()) {
                                users.add(res.result().get(0));
                            }
                            response.setStatusCode(HttpResponseStatus.OK.code()).end(users.encodePrettily());
                            return;
                        } else {
                            JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "DB error");
                            response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                            return;
                        }
                    });
                }
                else  {
                    JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "Bad token");
                    response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                    return;
                }
            }
            catch (Exception e) {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "Connection error");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                return;
            }
        });
    }


    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "mail": "mail@mail.com", "password": "admin"}' http://0.0.0.0:8081/register
     **/

    public void addUser(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        String username = requestAsJson.getString("username");
        String mail = requestAsJson.getString("mail");
        String password = requestAsJson.getString("password");

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("username", username);

        mongoClient.find("users", query, res0-> {

            if (res0.succeeded() && res0.result().isEmpty()) {
                JsonObject field = new JsonObject().put("username", username).put("mail", mail).put("password", password);

                mongoClient.insert("users", field, res -> {
                    getToken(context);
                    return;
//                    JsonObject responseAsJSON = new JsonObject().put("code", "200");
//                    response.setStatusCode(200).end(responseAsJSON.encode());

                });
            }
            else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("message", "username already exist");
                response.setStatusCode(HttpResponseStatus.CONFLICT.code()).end(responseAsJSON.encode());
            }
        });

    }

    /**
      * curl -H "Content-Type: application/json" -X DELETE -d '{"username": "admin", "password": "admin"}' http://0.0.0.0:8081/user
     **/

    public void deleteUser(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();


        String username = requestAsJson.getString("username");
        String password = requestAsJson.getString("password");


        MongoClient mongoClient = Client.INSTANCE.getMongoClient();


        JsonObject field = new JsonObject().put("username", username).put("password", password);


        mongoClient.removeDocument("users", field, res -> {

            if (res.succeeded()) {
                JsonObject responsAsJSON = new JsonObject().put("result", "success");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responsAsJSON.encode());
                return;
            }
            else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }
        });
    }

    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "admin"}' http://0.0.0.0:8081/login
     **/


    public void getToken(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        String username = requestAsJson.getString("username");
        String password = requestAsJson.getString("password");


        MongoClient mongoClient = Client.INSTANCE.getMongoClient();
        /**
         * User to generate token
         */
        JsonObject query = new JsonObject().put("username", username).put("password", password);


        mongoClient.find("users", query, resUser-> {
           if (resUser.succeeded() &&  !resUser.result().isEmpty()){

               /**
                * Request to generate token
                */
               String token = generateString();
               JsonObject update = new JsonObject().put("$set", new JsonObject().put("token", token));



               mongoClient.updateCollection("users", query, update, res -> {
                   if (res.succeeded()) {
                       JsonObject responsAsJSON = new JsonObject().put("result", "success").put("token", token);
                       response.setStatusCode(HttpResponseStatus.OK.code()).end(responsAsJSON.encode());
                       return;
                   }
                   else {
                       JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "DB ERROR");
                       response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                   }
               });

           }
           else {
               JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("message", "bad username or password");
               response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code()).end(responseAsJSON.encode());

           }
        });

    }


    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "admin", "new_password": "pass2"}' http://0.0.0.0:8081/user/pass
     **/

    public void changePasswordUser(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();


        /**
         ** Recup var from JSON
         **/
        String username = requestAsJson.getString("username");
        String password = requestAsJson.getString("password");
        String new_password = requestAsJson.getString("new_password");


        /**
         ** Recup client MONGODB
         **/
        MongoClient mongoClient = Client.INSTANCE.getMongoClient();


        /**
         * User to change password
         */
        JsonObject query = new JsonObject().put("username", username).put("password", password);

        /**
         * Request to change password
         */
        JsonObject update = new JsonObject().put("$set", new JsonObject().put("password", new_password));

        /**
         ** injection
         **/
        mongoClient.updateCollection("users", query, update, res -> {

            /**
             ** Callback
             **/
            if (res.succeeded()) {
                JsonObject responseAsJSON = new JsonObject().put("result", "success");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
            } else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }
        });
    }

    /**
     * Make user Admin
     * curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "admin", "master_password": "pass"}' http://0.0.0.0:8081/user/admin
     */

    public void userAdmin(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();


        /**
         ** Recup var from JSON
         **/
        String username = requestAsJson.getString("username");
        String password = requestAsJson.getString("password");
        String masterPassword = requestAsJson.getString("master_password");

        if (masterPassword.compareTo("admin!") != 0) {
            JsonObject responseAsJSON = new JsonObject().put("error", "wrong master password");
            response.setStatusCode(200).end(responseAsJSON.encode());
            return;
        }
        /**
         ** Recup client MONGODB
         **/
        MongoClient mongoClient = Client.INSTANCE.getMongoClient();


        /**
         * User to change admin status
         */
        JsonObject query = new JsonObject().put("username", username).put("password", password);

        /**
         * Request to change admim value
         */
        JsonObject update = new JsonObject().put("$set", new JsonObject().put("admin", 1));

        mongoClient.updateCollection("users", query, update, res-> {
            if (res.succeeded()) {
                JsonObject responseAsJSON = new JsonObject().put("result", "success");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
            } else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }
        });

    }

    /**
     * Make user non Admin
     * curl -H "Content-Type: application/json" -X DELETE -d '{"username": "admin", "password": "admin", "master_password": "pass"}' http://0.0.0.0:8081/user/admin
     */

    public void userNonAdmin(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();


        /**
         ** Recup var from JSON
         **/
        String username = requestAsJson.getString("username");
        String password = requestAsJson.getString("password");
        String masterPassword = requestAsJson.getString("master_password");

        System.out.println(masterPassword);
        if (masterPassword.compareTo("admin!") != 0) {
            JsonObject responseAsJSON = new JsonObject().put("error", "wrong master password");
            response.setStatusCode(200).end(responseAsJSON.encode());
            return;
        }

        /**
         ** Recup client MONGODB
         **/
        MongoClient mongoClient = Client.INSTANCE.getMongoClient();


        /**
         * User to change admin status
         */
        JsonObject query = new JsonObject().put("username", username).put("password", password);


        /**
         * Request to change admim value
         */
        JsonObject update = new JsonObject().put("$set", new JsonObject().put("admin", 0));

        mongoClient.updateCollection("users", query, update, res-> {
            if (res.succeeded()) {
                JsonObject responseAsJSON = new JsonObject().put("result", "success");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
            } else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }

        });
    }

    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "id": "123456789"}' http://0.0.0.0:8081/users/favorites/pattterns
     */


    public void addFavPattern(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        String token = requestAsJson.getString("token");
        String id = requestAsJson.getString("id");

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("token", token);

//        JsonObject update = new JsonObject().put("$push", new JsonObject().put("favPattern", new JsonObject().put("id", id)));
        JsonObject update = new JsonObject().put("$push", new JsonObject().put("favPattern", id));

        mongoClient.updateCollection("users", query, update, res -> {
            if (res.succeeded()) {
                JsonObject responseAsJSON = new JsonObject().put("result", "success").put("message", id + " add int fav");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
            } else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "connection error");
                response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code()).end(responseAsJSON.encode());
            }

        });
    }

    /**
     * curl -H "Content-Type: application/json" -X DELETE-d '{"token": "putrighttoken", "id": "123456789"}' http://0.0.0.0:8081/users/favorites/pattterns
     */

    public void delFavPattern(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        String token = requestAsJson.getString("token");
        String id = requestAsJson.getString("id");

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("token", token);

//        JsonObject update = new JsonObject().put("$pull", new JsonObject().put("favPattern", new JsonObject().put("id", id)));
        JsonObject update = new JsonObject().put("$pull", new JsonObject().put("favPattern", id));

        mongoClient.updateCollection("users", query, update, res -> {
           if (res.succeeded()) {
               JsonObject responseAsJSON = new JsonObject().put("result", "success").put("message", id + " remove of fav for user ");
               response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
           } else {
               JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "unknown error");
               response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(requestAsJson.encode());
           }
        });
    }
    /**
     ** curl -H "Content-Type: application/json" -X GET -d '{"token": "putrighttoken"}' http://0.0.0.0:8081/users/favorites/pattterns
    **/

    public void getFavPattern(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        String token = requestAsJson.getString("token");
        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("token", token);

        mongoClient.find("users", query, res -> {
            if (res.succeeded()) {
                System.out.println(res.result().get(0).encodePrettily());
                JsonObject responseAsJSON = new JsonObject().put("result", "success").put("favorite", res.result().get(0).getJsonArray("favPattern").toString());
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
            } else {
                JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "connection error");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }

        });
    }

}