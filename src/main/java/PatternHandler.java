import com.mongodb.MongoException;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;
import jdk.nashorn.api.scripting.JSObject;
import mongo.Client;
import util.MongoDbObjectIdToId;

import java.awt.*;
import java.util.List;

public class PatternHandler {
    /**
     * * curl -H "Content-Type: application/json" -X POST -d '{"token": "token", "pattern": "Do food"}' http://0.0.0.0:8081/patterns
     **/

    public void addPattern(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();


        JsonObject patternJson = new JsonObject();
        try {
            patternJson.put("Title", requestAsJson.getString("Title"));
            patternJson.put("Description", requestAsJson.getString("Description"));
            patternJson.put("Problem", requestAsJson.getString("Problem"));
            patternJson.put("Solution", requestAsJson.getString("Solution"));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }
        if (patternJson.isEmpty()) {
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
            return;
        }

        try {
            mongoClient.insert("pattern", patternJson, res2 ->
            {
                if (res2.succeeded()) {
                    JsonObject responseAsJSON = new JsonObject().put("result", "success");
                    response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                } else {
                    JsonObject responseAsJSON = new JsonObject().put("result", "fail");
                    response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("DB Insert Error");
        }
    }



    /**
     *
     *
     * db.getCollection('users').aggregate([
     { $project :
     { "_id" : 0 , "favPattern" : 1 }
     } ,

     {"$unwind" : "$favPattern"}
     ,
     {"$lookup": {
     "from" : "pattern",
     "localField": "favPattern",
     "foreignField": "_id",
     "as": "patterns"
     }
     },
     {"$unwind" : "$patterns"}
     ,
     {
     $replaceRoot: { newRoot: "$patterns"}
     }
     ])
     */

    /**
     * *   Get all patternx
     * *   curl  http://0.0.0.0:8081/patterns?token=a1b2c3
     **/

    public void getAllPatterns(RoutingContext context) {
        HttpServerResponse response = context.response();


        String token = context.request().getParam("token");


        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject queryCo = new JsonObject().put("token", token);

        JsonArray resPattern = new JsonArray();
        mongoClient.find("users", queryCo, res -> {
            if (res.succeeded()) {
                JsonObject query = new JsonObject();
                mongoClient.find("pattern", query, res2 -> {
                    if (res2.succeeded()) {
                        //ici
                        try {

                            if (res.result().get(0).containsKey("favPattern")) {

                            JsonArray fav = res.result().get(0).getJsonArray("favPattern");
                            List<JsonObject> pattern = res2.result();
                            for (JsonObject s : pattern) {
                                JsonObject tmp = new JsonObject(s.toString());
                                for (int i = 0; i < fav.size(); i++) {
                                    String oneFave = fav.getString(i);

                                    if (tmp.getString("_id").equals(oneFave)) {
                                        tmp.put("fav", "true");
                                    }

                                }
                                if (!tmp.containsKey("fav")) {
                                    tmp.put("fav", "false");
                                }


                                resPattern.add(tmp);


                            }
                            response.setStatusCode(HttpResponseStatus.OK.code());
                            response.end(resPattern.toString());
                        }
                            else{
                            mongoClient.find("pattern", new JsonObject(), res3 -> {
                                if (res3.succeeded()) {
                                    response.setStatusCode(HttpResponseStatus.OK.code());
                                    response.end(res3.result().toString());
                                }
                            });
                        }
                        }
                        catch(IndexOutOfBoundsException e) {
                            mongoClient.find("pattern", new JsonObject(), res4 -> {
                                if (res4.succeeded()) {
                                    System.out.println(res4.result());
                                    response.setStatusCode(HttpResponseStatus.OK.code());
                                    response.end(res4.result().toString());
                                }
                            });
                        }
                    } ;
                });


            }
        });
    }


    public void getSinglePattern(RoutingContext context) {
        HttpServerResponse response = context.response();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();
        String id = context.request().getParam("patternId");

        JsonObject query = new JsonObject();
        System.out.print(id);
        query.put("_id", new JsonObject().put("$oid", id));


        mongoClient.findOne("pattern", query, null, res -> {

            System.out.println(res.result());
            if (res.result() != null) {
                response.setStatusCode(HttpResponseStatus.OK.code());
                response.end(MongoDbObjectIdToId.ReplaceOidWithId(res.result()).encode());
            } else {
                response.setStatusCode(HttpResponseStatus.NOT_FOUND.code());
                response.end("no pattern found");
            }
        });
    }

    /**
     * *   Remove Pattern
     * * curl -H "Content-Type: application/json" -X DELETE -d '{"token": "token", "id": "id"}' http://0.0.0.0:8081/patterns/
     **/

    public void deletePattern(RoutingContext context) {
        JsonObject requestAsJson = context.getBodyAsJson();


        String id = requestAsJson.getString("id");
        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        try {
            JsonObject field = new JsonObject().put("_id", id);
            mongoClient.removeDocument("pattern", field, res2 -> {
                if (res2.succeeded()) {
                    System.out.println(res2.result());
                    JsonObject responseAsJSON = new JsonObject().put("message", id + " deleted");
                    HttpServerResponse response = context.response();
                    response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encodePrettily());
                }

            });
        } catch (MongoException e) {
            HttpServerResponse response = context.response();
            JsonObject responseAsJSON = new JsonObject().put("Error", " DB error ");
            response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(responseAsJSON.encodePrettily());
        }
    }
}

