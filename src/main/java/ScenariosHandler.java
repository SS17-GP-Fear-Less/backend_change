import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.parsetools.impl.RecordParserImpl;
import io.vertx.ext.auth.User;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.RoutingContext;
import mongo.Client;

import java.security.acl.LastOwnerException;
import java.util.UUID;

public class ScenariosHandler {


    private static int lenJsonArray(JsonArray jsonArray) {
        int size = 0;

        for (Object s : jsonArray) {
            size++;
        }
        return size;
    }

    /**
     * * curl -H "Content-Type: application/json" -X POST -d '{"token": "token", "title": "title", "description": "String"}' http://0.0.0.0:8081/users/scenarios
     **/

    public void addScenarios(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

//        System.out.println(context.getBodyAsJson().encodePrettily());

        String token = requestAsJson.getString("token");
        JsonObject scenJson = new JsonObject();
        try {
            scenJson.put("title", requestAsJson.getString("title"));
            scenJson.put("description", requestAsJson.getString("description"));
            scenJson.put("isResolved", 0);
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }
        if (scenJson.isEmpty()) {
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
            return;
        }

        try {
            JsonObject query = new JsonObject().put("token", token);

            mongoClient.find("users", query, res -> {
                if (res.succeeded()) {
                    scenJson.put("userId", res.result().get(0).getString("_id"));

                    mongoClient.insert("scenarios", scenJson, res2 ->
                    {
                        if (res2.succeeded()) {
                            JsonObject responseAsJSON = new JsonObject().put("result", "success");
                            response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                        } else {
                            JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("error", "DB error");
                            response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("DB Insert Error");
        }
    }

    /**
     * * curl -H "Content-Type: application/json" -X DELETE -d '{"token": "token", "id":"ab2b3c44"}' http://0.0.0.0:8081/users/scenarios
     **/

    public void delScenarios(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();
        String token = requestAsJson.getString("token");

        JsonObject scenJson = new JsonObject();
        try {
            scenJson.put("_id", requestAsJson.getString("id"));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }
        if (scenJson.isEmpty()) {
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
            return;
        }
        try {
            JsonObject query = new JsonObject().put("token", token);

            mongoClient.find("users", query, res -> {
                if (res.succeeded()) {
                    scenJson.put("userId", res.result().get(0).getString("_id"));

                    mongoClient.removeDocument("scenarios", scenJson, res2 ->
                    {
                        if (res2.succeeded()) {
                            JsonObject responseAsJSON = new JsonObject().put("code", "200");
                            response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                        } else {
                            JsonObject responseAsJSON = new JsonObject().put("error", "DB error");
                            response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("DB remove Error");
        }
    }

    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenario": "123456789", "comment" : "comment text"}' http://0.0.0.0:8081/users/scenarios/comments
     */

    public void addCommentToScenario(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();
        String token = "";

        try {
            token = requestAsJson.getString("token");
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("token", token);


        mongoClient.find("users", query, res -> {
            if (res.succeeded()) {
                String login = res.result().get(0).getString("_id");
                String username = res.result().get(0).getString("username");
                String stratId = UUID.randomUUID().toString().replace("-", "");

                JsonObject query2 = new JsonObject().put("userId", login).put("_id", requestAsJson.getString("idScenario"));

                JsonObject update = new JsonObject().put("$push", new JsonObject().put("comments", new JsonObject()
                        .put("comment", requestAsJson.getString("comment"))
                        .put("username", username)
                        .put("id", stratId)));

                System.out.println(query2.encodePrettily());
                System.out.println(update.encodePrettily());

                mongoClient.updateCollection("scenarios", query2, update, res2 -> {
                    if (res2.succeeded()) {
                        JsonObject responseAsJSON = new JsonObject().put("result", "success").put("message", "comment added");
                        response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                    } else {
                        JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("message", "connection error");
                        response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code()).end(responseAsJSON.encode());
                    }

                });
            }
        });
    }

    /**
     * http://0.0.0.0:8081/users/scenarios/comment?token=putrighttoken&idScenario=123456
     * * GET COMMENT OF A SCENARIO
     */

    public static void GetCommentOfStrategie(RoutingContext context) {
        HttpServerResponse response = context.response();
        String token = "";
        try {
            token = context.request().getParam("token");
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        String login = UserHandler.getUserbyKey(token);

        JsonObject query = new JsonObject().put("token", token);
        mongoClient.find("users", query, resUser -> {
            if (resUser.succeeded()) {
                if (context.request().getParam("idScenario") != null) {
                    JsonArray strats = new JsonArray();
                    JsonObject query2 = new JsonObject().put("_id", context.request().getParam("idScenario"));
                    mongoClient.find("scenarios", query2, res -> {
                        if (res.succeeded() && !res.result().isEmpty()) {
                            JsonObject t = JsonObject.mapFrom(res.result().get(0));
                            String scenariosID = t.getString("_id");
                            try {

                                for (int i = 0; i < t.getJsonArray("comments").size(); i++) {
                                    JsonObject strat = new JsonObject();

                                    Object s = t.getJsonArray("comments").getJsonObject(i);
                                    JsonObject stratMap = JsonObject.mapFrom(s);
                                    strat.put("commentId", stratMap.getString("id"))
                                            .put("comment", stratMap.getString("comment"))
                                            .put("username", resUser.result().get(0).getString("username"));
                                    strats.add(strat);
                                    System.out.println(strat.encodePrettily());

                                }
                            } catch (Exception e) {
                            }

                        }


                        if (strats.isEmpty()) {
                            response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(new JsonObject().put("result", "empty").toString());
                            return;
                        }
                        response.setStatusCode(HttpResponseStatus.OK.code()).end(strats.toString());

                    });


                    return;
                }

                JsonObject responseAsJSON = new JsonObject().put("result", "failed");
                response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(responseAsJSON.encode());
            }
        });
    }


    /**
     * curl -H "Content-Type: application/json" -X DELETE -d '{"token": "putrighttoken", "idScenario": "123456789", "strategies": "123456789,987654321"}' http://0.0.0.0:8081/users/scenarios/strategies
     */


    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenario": "123456789", "title": "titledesc1", "description": "desc", "patterns": "123456789,987654321"}' http://0.0.0.0:8081/users/scenarios/strategies
     */


    public void addStrategieToScenario(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();
        String token = "";
        try {
            token = requestAsJson.getString("token");
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("token", token);


        mongoClient.find("users", query, res -> {
            if (res.succeeded()) {
                String login = res.result().get(0).getString("_id");
                String username = res.result().get(0).getString("username");
                String stratId = UUID.randomUUID().toString().replace("-", "");

                JsonObject query2 = new JsonObject().put("userId", login).put("_id", requestAsJson.getString("idScenario"));

                JsonObject update = new JsonObject().put("$push", new JsonObject().put("strategies", new JsonObject()
                        .put("strategies", requestAsJson.getString("patterns"))
                        .put("title", requestAsJson.getString("title"))
                        .put("description", requestAsJson.getString("description"))
                        .put("username", username)
                        .put("id", stratId)));

                System.out.println(query2.encodePrettily());
                System.out.println(update.encodePrettily());

                mongoClient.updateCollection("scenarios", query2, update, res2 -> {
                    if (res2.succeeded()) {
                        JsonObject responseAsJSON = new JsonObject().put("result", "success").put("message", "strat added");
                        response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                    } else {
                        JsonObject responseAsJSON = new JsonObject().put("result", "fail").put("message", "connection error");
                        response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code()).end(responseAsJSON.encode());
                    }

                });
            }
        });
    }

    /**
     * curl -H "Content-Type: application/json" -X DELETE -d '{"token": "putrighttoken", "idScenario": "123456789", "strategies": "123456789,987654321"}' http://0.0.0.0:8081/users/scenarios/strategies
     */

    public void delStrategieInScenario(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();
        String token = "";
        try {
            token = requestAsJson.getString("token");
            ;
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        JsonObject query = new JsonObject().put("token", token);

        mongoClient.find("users", query, res -> {
            if (res.succeeded()) {
                String login = res.result().get(0).getString("_id");


                JsonObject update = new JsonObject().put("$pull", new JsonObject().put("strategies", requestAsJson.getString("strategies")));

                JsonObject query2 = new JsonObject().put("userId", login).put("_id", requestAsJson.getString("idScenario"));

                System.out.println("DEL STRAT");
                System.out.println(query2);

                mongoClient.updateCollection("scenarios", query2, update, res2 -> {
                    if (res2.succeeded()) {
                        JsonObject responseAsJSON = new JsonObject().put("message", "strat deleted");
                        response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                    } else {
                        JsonObject responseAsJSON = new JsonObject().put("error", "connection error");
                        response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code()).end(responseAsJSON.encode());
                    }

                });
            }
        });
    }


    /**
     * http://0.0.0.0:8081/users/scenarios/token=putrighttoken
     * * REMOVE TOKEN AND CONECTION
     */

    public static void getScenarios(RoutingContext context) {
        HttpServerResponse response = context.response();
        String token = "";
        try {
            token = context.request().getParam("token");
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();


        JsonObject query = new JsonObject();

        mongoClient.find("users", query, resUser -> {
            if (resUser.succeeded()) {
                JsonObject query2 = new JsonObject().put("userId", resUser.result().get(0).getString("_id"));
                mongoClient.find("scenarios", query2, res -> {
                    if (res.succeeded() && !res.result().isEmpty()) {
                        JsonArray scenarios = new JsonArray();
                        for (JsonObject s : res.result()) {
                            s.getJsonArray("strategies");
                            int len = 0;
                            if (s.containsKey("strategies")) {
                                len = lenJsonArray(s.getJsonArray("strategies"));
                            }
                            JsonObject scen = new JsonObject().put("_id", s.getString("_id"))
                                    .put("title", s.getString("title"))
                                    .put("description", s.getString("description"))
                                    .put("isResolved", s.getInteger("isResolved"))
                                    .put("userId", s.getString("userId"))
                                    .put("strategies", len);
                            scenarios.add(scen);
                        }

                        response.setStatusCode(HttpResponseStatus.OK.code()).end(scenarios.toString());
                    } else {
                        JsonObject responseAsJSON = new JsonObject().put("result", "failed");
                        response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(responseAsJSON.encode());
                    }
                });
            }
        });
    }

    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenario": "123456789"}' http://0.0.0.0:8081/users/scenarios/resolves
     */

    public void scenariosResolve(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        String login = "";
        String idScenario = "";
        try {
            login = requestAsJson.getString("login");
            idScenario = requestAsJson.getString("idScenario");

        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        JsonObject query = new JsonObject().put("userlogin", login).put("_id", idScenario);


        JsonObject update = new JsonObject().put("$set", new JsonObject().put("isResolved", 1));

        mongoClient.updateCollection("users", query, update, res -> {
            if (res.succeeded()) {
                JsonObject responseAsJSON = new JsonObject().put("code", "200");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                return;
            } else {
                JsonObject responseAsJSON = new JsonObject().put("error", "DB ERROR");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }
        });
    }


    /**
     * curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenarios": "123456789", "idStrategie": "123456789"}' http://0.0.0.0:8081/user/scenarios/unresolve
     */


    public void scenariosNotResolve(RoutingContext context) {
        HttpServerResponse response = context.response();
        JsonObject requestAsJson = context.getBodyAsJson();

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        String login = "";
        String idScenario = "";
        try {
            login = requestAsJson.getString("login");
            idScenario = requestAsJson.getString("idScenario");

        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        JsonObject query = new JsonObject().put("userlogin", login).put("_id", idScenario);


        JsonObject update = new JsonObject().put("$set", new JsonObject().put("isResolved", 0));

        mongoClient.updateCollection("users", query, update, res -> {
            if (res.succeeded()) {
                JsonObject responseAsJSON = new JsonObject().put("code", "200");
                response.setStatusCode(HttpResponseStatus.OK.code()).end(responseAsJSON.encode());
                return;
            } else {
                JsonObject responseAsJSON = new JsonObject().put("error", "DB ERROR");
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end(responseAsJSON.encode());
            }
        });

    }

    /**
     * http://0.0.0.0:8081/users/scenarios/strategies?token=putrighttoken&idScenario=123456
     * * GET STRATEGIE OF A SCENARIO
     */

    public static void getStrategieOfScenario(RoutingContext context) {
        HttpServerResponse response = context.response();
        String token = "";
        try {
            token = context.request().getParam("token");
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
            response.end("Wrong Pattern Format");
        }

        MongoClient mongoClient = Client.INSTANCE.getMongoClient();

        String login = UserHandler.getUserbyKey(token);

        JsonObject query = new JsonObject().put("token", token);
        mongoClient.find("users", query, resUser -> {
            if (resUser.succeeded()) {
                if (context.request().getParam("idScenario") == null) {
                    JsonArray strats = new JsonArray();
                    JsonObject query2 = new JsonObject();//.put("userId", resUser.result().get(0).getString("_id"));
                    mongoClient.find("scenarios", query2, res -> {
                        if (res.succeeded() && !res.result().isEmpty()) {
                            for (Object o : res.result()) {
                                JsonObject t = JsonObject.mapFrom(o);
                                String scenariosID = t.getString("_id");
                                try {

                                    for (int i = 0; i < t.getJsonArray("strategies").size(); i++) {
                                        JsonObject strat = new JsonObject();

                                        Object s = t.getJsonArray("strategies").getJsonObject(i);
                                        JsonObject stratMap = JsonObject.mapFrom(s);
                                        strat.put("strategieID", stratMap.getString("id"))
                                                .put("scenarioID", scenariosID)
                                                .put("title", stratMap.getString("title"))
                                                .put("patterns", stratMap.getString("strategies"))
                                                .put("description", stratMap.getString("description"))
                                                .put("username", resUser.result().get(0).getString("username"));
                                        strats.add(strat);
                                        System.out.println(strat.encodePrettily());

                                    }
                                } catch (Exception e) {
                                }

                            }

                        }
                        if (strats.isEmpty()) {
                            response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(new JsonObject().put("result", "empty").toString());
                            return;
                        }
                        response.setStatusCode(HttpResponseStatus.OK.code()).end(strats.toString());

                    });


                    return;
                }

                JsonObject query2 = new JsonObject().put("userId", resUser.result().get(0).getString("_id"))
                        .put("_id", context.request().getParam("idScenario"));

                mongoClient.find("scenarios", query2, res -> {
                    if (res.succeeded() && !res.result().isEmpty()) {
                        JsonObject s = res.result().get(0);
                        if (s.containsKey("strategies") == false) {
                            JsonObject responseAsJSON = new JsonObject().put("result", "failed").put("error", "empty");
                            response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(responseAsJSON.encode());
                            return;
                        }

                        if (s.containsKey("strategies")) {
                            JsonArray strat = new JsonArray();
                            JsonArray Vstrats = s.getJsonArray("strategies");
                            for (Object v : Vstrats) {
                                JsonObject t = JsonObject.mapFrom(v);
                                System.out.println(t.encodePrettily());
                                strat.add(new JsonObject().put("title", t.getString("title")).put("patterns", t.getString("strategies")).put("description", t.getString("description")).put("username", resUser.result().get(0).getString("username")));
                            }
                            response.setStatusCode(HttpResponseStatus.OK.code()).end(strat.toString());
                        } else {
                            JsonObject responseAsJSON = new JsonObject().put("result", "failed");
                            response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(responseAsJSON.encode());
                        }
                    } else {
                        JsonObject responseAsJSON = new JsonObject().put("result", "failed");
                        response.setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end(responseAsJSON.encode());
                    }
                });
            }
        });
    }

}
