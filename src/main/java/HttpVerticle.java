
import mongo.Client;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import util.AuthenticationHelper;


/**
 * This class is our vert.x verticle, we extend AbstractVerticle.
 * AbstractVerticle: An abstract base class that you can extend to write your own Verticle classes.
 */
public class HttpVerticle extends AbstractVerticle {

    private UserHandler user;
    private PatternHandler pattern;
    private ScenariosHandler scenario;

    private AuthenticationHelper addPatternifAdmin;
    private AuthenticationHelper removePatternIfAdmin;
    private AuthenticationHelper addScenarioIfUser;
    private AuthenticationHelper delScenarioIfUser;
    private AuthenticationHelper addStrategieToScenarioIfUser;
    private AuthenticationHelper delStrategieInScenarioIfUser;
//    private AuthenticationHelper getScenarioOfUserIfUser;
    private AuthenticationHelper scenariosResolveIfUser;
    private AuthenticationHelper scenariosNotResolveIfUser;
    private AuthenticationHelper scenariosAddCommentIfUser;

    /**
     * This method is the start of a verticle.
     */
    @Override
    public void start() {

        user = new UserHandler();
        pattern = new PatternHandler();
        scenario = new ScenariosHandler();

        addPatternifAdmin = new AuthenticationHelper(pattern::addPattern);
        removePatternIfAdmin = new AuthenticationHelper(pattern::deletePattern);
        addScenarioIfUser = new AuthenticationHelper(scenario::addScenarios);
        delScenarioIfUser = new AuthenticationHelper(scenario::delScenarios);
        addStrategieToScenarioIfUser = new AuthenticationHelper(scenario::addStrategieToScenario);
        delStrategieInScenarioIfUser = new AuthenticationHelper(scenario::delStrategieInScenario);

        scenariosResolveIfUser = new AuthenticationHelper(scenario::scenariosResolve);
        scenariosNotResolveIfUser = new AuthenticationHelper(scenario::scenariosNotResolve);
        scenariosAddCommentIfUser = new AuthenticationHelper(scenario::addCommentToScenario);

        Router router = setupRouter();
        HttpServer server = vertx.createHttpServer();
        server.requestHandler(router::accept).listen(8081);



        if (this.config().getString("db") != null) {
            System.out.println(this.config().getString("db"));
            Client.INSTANCE.getConfig().put("host", config().getString("db"));
        }


        Client.INSTANCE.init(vertx);

        System.out.println(this.config().getString("db"));

        System.out.println("We start listening");
    }

    /**
     * Setup our router to dispatch request to different class.
     */
    private Router setupRouter() {
        Router router = Router.router(vertx);

        // Define our router to use only application/json
        router.route().consumes("application/json");
        router.route().produces("application/json");
        router.route().handler(BodyHandler.create());
        router.route().handler(context -> {
            context.response().headers().add("Content-Type", "application/json");
            context.next();
        });



        router.post("/register/").handler(user::addUser);  // curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "admin"}' http://0.0.0.0:8081/register
        router.delete("/users/").handler(user::deleteUser);  // curl -H "Content-Type: application/json" -X DELETE -d '{"username": "admin", "password": "admin"}' http://0.0.0.0:8081/users
        router.get("/users/").handler(user::getAllUser); // curl -H "Content-Type: application/json" -X GET -d '{"token": "1234"}' http://0.0.0.0:8081/users
        router.post("/users/pass/").handler(user::changePasswordUser);  // curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "admin", "new_password": "pass2"}' http://0.0.0.0:8081/user/pass
        router.post("/users/admin/").handler(user::userAdmin); // curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "admin", "master_password": "admin!"}' http://0.0.0.0:8081/user/admin
        router.delete("/users/admin/").handler(user::userNonAdmin); // curl -H "Content-Type: application/json" -X DELETE -d '{"username": "admin", "password": "admin", "master_password": "admin!"}' http://0.0.0.0:8081/user/admin
        router.post("/login").handler(user::getToken); // curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "mail": "mail@mail.com", "password": "admin"}' http://0.0.0.0:8081/username
        router.post("/users/favorites/patterns").handler(user::addFavPattern); // curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "id": "123456789"}' http://0.0.0.0:8081/user/favorites/pattterns
        router.get("/users/favorites/patterns").handler(user::getFavPattern); // curl -H "Content-Type: application/json" -X GET -d '{"token": "putrighttoken"}' http://0.0.0.0:8081/users/favorites/pattterns
        router.delete("/users/favorites/patterns").handler(user::delFavPattern); // curl -H "Content-Type: application/json" -X DELETE-d '{"token": "putrighttoken", "id": "123456789"}' http://0.0.0.0:8081/user/favorites/patterns

        router.post("/patterns/").handler(addPatternifAdmin::isAdminBeforeHanlde);  //  curl -H "Content-Type: application/json" -X POST -d '{"token": "admin", "getAllPatterns": "Do food"}' http://0.0.0.0:8081/pattern
        router.get("/patterns/").handler(pattern::getAllPatterns);    //  curl  http://0.0.0.0:8081/pattern?token=2af5cbf7c9994bda8764b9d00652970b
        router.get("/patterns/:patternId").handler(pattern::getSinglePattern);    //  curl  http://0.0.0.0:8081/pattern
        router.delete("/patterns").handler(removePatternIfAdmin::isAdminBeforeHanlde); // curl -H "Content-Type: application/json" -X DELETE -d '{"token"https://www.getpostman.com/collections/c0ba4cd6778b749cb925: "token", "id": "5b0f2a12e4cd7a56f21d8868"}' http://0.0.0.0:8081/pattern/

        router.post("/users/scenarios").handler(addScenarioIfUser::isUserBeforeHanlde); // curl -H "Content-Type: application/json" -X POST -d '{"token": "token", "title": "title", "description": "String"}' http://0.0.0.0:8081/users/scenarios
        router.delete("/users/scenarios").handler(delScenarioIfUser::isUserBeforeHanlde); // curl -H "Content-Type: application/json" -X DELETE -d '{"token": "token", "id":"ab2b3c44"}' http://0.0.0.0:8081/users/scenarios
        router.post("/users/scenarios/strategies").handler(addStrategieToScenarioIfUser::isUserBeforeHanlde); // curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenario": "123456789", "title": "titledesc1", "description": "desc", "patterns": "123456789,987654321"}' http://0.0.0.0:8081/users/scenarios/strategies
        router.delete("/users/scenarios/strategies").handler(delStrategieInScenarioIfUser::isUserBeforeHanlde); // curl -H "Content-Type: application/json" -X DELETE -d '{"token": "putrighttoken", "idScenario": "123456789", "strategies": "123456789,987654321"}' http://0.0.0.0:8081/users/scenarios/strategies
        router.get("/users/scenarios/strategies").handler(ScenariosHandler::getStrategieOfScenario);  //getStrategieOfScenario
        router.get("/users/scenarios").handler(ScenariosHandler::getScenarios); // curl http://0.0.0.0:8081/users/scenarios?token=2af5cbf7c9994bda8764b9d00652970b
        router.post("/users/scenarios/resolve").handler(scenariosResolveIfUser::isUserBeforeHanlde); // curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenario": "123456789"}' http://0.0.0.0:8081/users/scenarios/resolve
        router.post("/users/scenarios/unresolve").handler(scenariosNotResolveIfUser::isUserBeforeHanlde); // curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "idScenarios": "123456789", "idStrategie": "123456789"}' http://0.0.0.0:8081/users/scenarios/unresolve
        router.post("/users/scenarios/comments").handler(scenariosAddCommentIfUser::isUserBeforeHanlde);
        router.get("/users/scenarios/comments").handler(ScenariosHandler::GetCommentOfStrategie);
        return router;
    }
}
