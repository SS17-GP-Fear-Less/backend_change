#################################################  LOCAL VERSION

add user [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin"}' http://0.0.0.0:8081/users

delete user [DELETE]:
curl -H "Content-Type: application/json" -X DELETE -d '{"login": "admin", "password": "admin"}' http://0.0.0.0:8081/users

change password user [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin", "new_password": "pass2"}' http://0.0.0.0:8081/users/pass

make user admin [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin", "master_password": "admin!"}' http://0.0.0.0:8081/users/admin

make user non admin [DELETE]:
curl -H "Content-Type: application/json" -X DELETE -d '{"login": "admin", "password": "admin", "master_password": "admin!"}' http://0.0.0.0:8081/users/admin

liste of users [GET]:
curl -H "Content-Type: application/json" -X GET -d '{"token": "put the right token"}' http://0.0.0.0:8081/users

getToken / connect [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin"}' http://0.0.0.0:8081/users/token

add pattern [POST]:
curl -H "Content-Type: application/json" -X POST -d '{
                                                     	"token": "2af5cbf7c9994bda8764b9d00652970b",
                                                     	"Title" : "Help the Positive2",
                                                     	"Description" : "Long Description Text",
                                                     	"Problem" : "motivate them with a sense of hope rather than fear.",
                                                     	"Solution" : "Your attempts to scare others are not working."
                                                     }' http://0.0.0.0:8081/patterns

get all pattern [GET]:
curl  http://0.0.0.0:8081/patterns?token=a1b2c3

delete pattern [DELETE]:
curl -H "Content-Type: application/json" -X DELETE -d '{"token": "admin", "id": "5b0f2a12e4cd7a56f21d8868"}' http://0.0.0.0:8081/patterns

add pattern favorite [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "id": "123456789"}' http://0.0.0.0:8081/users/favorites/pattterns

get pattern favorite [GET]:
curl -H "Content-Type: application/json" -X GET -d '{"token": "putrighttoken"}' http://0.0.0.0:8081/users/favorites/pattterns

del pattern favorite [DELETE]:
curl -H "Content-Type: application/json" -X DELETE-d '{"token": "putrighttoken", "id": "123456789"}' http://0.0.0.0:8081/users/favorites/pattterns
#################################################  ONLINE VERSION

add user [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin"}' http://martimprey.fr/users

delete user [DELETE]:
curl -H "Content-Type: application/json" -X DELETE -d '{"login": "admin", "password": "admin"}' http://0.0.0.0:8081/users

change password user [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin", "new_password": "pass2"}' http://martimprey.fr/users/pass

make user admin [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin", "master_password": "admin!"}' http://martimprey.fr/users/admin

make user non admin [DELETE]:
curl -H "Content-Type: application/json" -X DELETE -d '{"login": "admin", "password": "admin", "master_password": "admin!"}' http://martimprey.fr/users/admin

liste of users [GET]:
curl -H "Content-Type: application/json" -X GET -d '{"token": "put the right token"}' http://0.0.0.0:8081/users

getToken / connect [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"login": "admin", "password": "admin"}' http://martimprey.fr/users/token

add pattern [POST]:
curl -H "Content-Type: application/json" -X POST -d '{
                                                     	"token": "2af5cbf7c9994bda8764b9d00652970b",
                                                     	"Title" : "Help the Positive2",
                                                     	"Description" : "Long Description Text",
                                                     	"Problem" : "motivate them with a sense of hope rather than fear.",
                                                     	"Solution" : "Your attempts to scare others are not working."
                                                     }' http://martimprey.fr/pattern

get all pattern [GET]:
curl  http://martimprey.fr/pattern?token=a1b2c3

delete pattern [DELETE]:
curl -H "Content-Type: application/json" -X DELETE -d '{"token": "admin", "id": "5b0f2a12e4cd7a56f21d8868"}' http://martimprey.fr/pattern

add pattern favorite [POST]:
curl -H "Content-Type: application/json" -X POST -d '{"token": "putrighttoken", "id": "123456789"}' http://martimprey.fr/users/favorites/pattterns

get pattern favorite [GET]:
curl -H "Content-Type: application/json" -X GET -d '{"token": "putrighttoken"}' http://martimprey.fr/users/favorites/pattterns

del pattern favorite [DELETE]:
curl -H "Content-Type: application/json" -X DELETE-d '{"token": "putrighttoken", "id": "123456789"}' http://martimprey.fr/users/favorites/pattterns
